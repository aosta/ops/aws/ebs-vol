output "this_aws_ebs_vol_id" {
    value = aws_ebs_volume.ebs.id
}

output "this_aws_ebs_vol_arn" {
    value = aws_ebs_volume.ebs.arn
}
